import java.lang.*;

public class BoggleNode {
  private static boolean DEBUG = false;

  private char myChar;
  private boolean isAWord;
  private int ALPHABET_LENGTH = 26 + 1; // Add 1 for cardinality mismatch
  private BoggleNode[] nextLetter = new BoggleNode[ ALPHABET_LENGTH ];

  public boolean isValidPrefix( String prefix ) {
	if( DEBUG ) {
  	  System.out.println( "(NODE)(isValidPrefix) Do I hold '" + prefix + "'?" );
  	}
  	BoggleNode currNode = this;
    // For every letter of the given prefix...
  	for( int i = 0 ; i < prefix.length() ; i++ ) {
  	  char currChar = prefix.charAt( i );
      if( DEBUG ) {
      	System.out.println( "(NODE)(isValidPrefix) Checking: " + currChar );
      }
      // Advance the current node to that letter
      currNode = currNode.getNext( currChar );
      // But if no letter exists on the tree...
      if( currNode == null ) {
        if( DEBUG ) {
      	  System.out.println( "(NODE)(isValidPrefix) Invalid prefix on: " + currChar );
        }
      	// Then it's not a valid prefix
      	return false;
      }
  	}
  	// If all letters were checked and we're still on the tree...
  	if( DEBUG ) {
  	  System.out.println( "(NODE)(isValidPrefix) All letters of the prefix searched." );
  	}
  	// Then this is a valid prefix
  	return true;
  }

  BoggleNode( char c ) {
    this.tare();
    this.myChar = c;
  }

  BoggleNode( ) {
    this.tare();
  }

  public int getAlphabetLength() {
    return this.ALPHABET_LENGTH;
  }

  public BoggleNode getNext( char c ) {
  	int index = this.convertToInt( c );
  	return this.nextLetter[ index ];
  }

  public char getChar() {
  	return this.myChar;
  }

  public boolean isAWord() {
  	return this.isAWord;
  }

  // Asks if the node is the head of the word
  public boolean isWordYouHold( String word ) {
  	if( DEBUG ) {
  	  System.out.println( "(NODE)(isWordYouHold) Do I hold '" + word + "'?" );
  	}
  	char currChar = word.charAt( 0 );
    // "ZEBRA" - If my char is not 'Z', I am not the head of that suffix
    if( this.myChar != currChar ) {
      if( DEBUG ) {
      	System.out.println( "(NODE)(isWordYouHold) Mismatch detected: " +this.myChar + " != " + currChar + ". Returning false." );
      }
      return false;
    }

    // This is the base case, the last letter 'A'
    // Char mismatch already checked
    if( word.length() == 1 ) {
      if( DEBUG ) {
      	System.out.println( "(NODE)(isWordYouHold) Base case reached: " + currChar + " == " + this.myChar );
        System.out.println( "(NODE)(isWordYouHold) Returning '" + this.isAWord + "'" );
      }
      return this.isAWord;
    }

    // But if I am 'Z', I have to ask my 'E' node the same question
    char nextLetter = word.charAt(1);
    BoggleNode nextNode = this.getNext( nextLetter );

    // If I have no 'E' node...
    if( nextNode == null ) {
      return false;
    }
    // Otherwise, pass the question along
    else {
      return nextNode.isWordYouHold( word.substring(1) );
    }
  }

  public int convertToInt( char c ) {
    char theChar = Character.toUpperCase( c );
    int retInt = (int)theChar - 64;
    if( this.DEBUG && false ) { // Verified to work: 2021.06.03
    	System.out.println( "Converting to int: " + c + " -> " + retInt );
    }
    return retInt;
  }

  public void append( char c ) {
    int position = this.convertToInt( c );
    this.nextLetter[position] = new BoggleNode( c );
  }

  public void append( String word ) {
  	if( DEBUG ) {
      System.out.println( this.myChar + " <- " + word );
  	}
  	// If there are no letters left in the string...
  	if( word.length() == 0 ) {
  	  this.isAWord = true;
  	  return;
  	}
    // Otherwise... get the letter to append ( (head) <- 'Z' )
    char currLetter = word.charAt( 0 );
    int currNumber = this.convertToInt( currLetter );
    // If this letter has not been seen as a child of this node before...
    if( this.nextLetter[ currNumber ] == null ) {
      // Attach the child node
      BoggleNode child = new BoggleNode( currLetter );
   	  this.nextLetter[ currNumber ] = child;
    }
    // Otherwise, this child has been seen previously...
    else {
    	// So do nothing
    }

    // Tell the child to do the same procedure with the string 1 shorter
    this.nextLetter[ currNumber ].append( word.substring(1) );
  }

  private void tare() {
  	for( int i = 0 ; i < this.nextLetter.length ; i++ ) {
      nextLetter[i] = null;
  	}
  	this.myChar = (char)0;
  	this.isAWord = false;
  }

  public void test() {
    System.out.println( "Taring..." );
  	this.tare();
  	System.out.println( "Reporting..." );
  	this.display();

  	System.out.println( "Appending..." );
  	this.append( 'E' );
  	this.display();
  	this.append( "EBRA" );
  	this.display();

  	BoggleNode bChild = this.nextLetter[5];
  	bChild.display();
  }

  public void display() {
  	System.out.println( this.toString() );
  }

  public String toString() {
    String retString = "";
    retString += "myChar : " + this.myChar  + "\n";
    retString += "isAWord: " + this.isAWord	+ "\n";
    for( int i = 1 ; i <= 26 ; i++ ) {
      if( nextLetter[i] != null ) {
    	retString += "[" + (char)(i+64) +"] : " + System.identityHashCode(nextLetter[i]) + "\n";
      }
    }
    return retString;
  }
//-------|---------|
// #debugPrint()
// Desc: Print a message if DEBUG mode is on
// Args: arg1 - The message to print
//-------|---------|
  private void debugPrint( String arg ) {
    if( DEBUG ) {
      System.out.println( arg );
    }
  }

//-------|---------|---------|---------|---------|---------|---------|---------|
// SETTERS / GETTERS
//-------|---------|---------|---------|---------|---------|---------|---------|

//-------|---------|
// #setDebugMode()
// Desc: Print a message if DEBUG mode is on
// Args: arg1 - The message to print
//-------|---------|
  public void setDebugMode( String arg ) {
    if( arg.equals( "ON" ) ) {
      this.DEBUG = true;
    }
    else if( arg.equals( "OFF" ) ) {
      this.DEBUG = false;
    }
    else {
      System.out.println( "(setDebug) Unrecognized input");
    }
  }

}