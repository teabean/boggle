import java.util.Scanner;

public class Main {
  public static boolean DEBUG = false;
  public static void main(String[] args) {
    TrieDictionary myDictionary = new TrieDictionary( "dictionary.txt" );
    BoggleBoard myBoard = new BoggleBoard();

    if( DEBUG ) {
      BoggleNode myBoggleNode = new BoggleNode();
      myBoggleNode.test();
    }
  
    boolean doExit = false;
    Scanner userInput = new Scanner( System.in );
    while( !doExit ) {
      menu();
      String selection = userInput.nextLine();

      // CONFIGURE BOARD
      if( selection.equals( "1" ) ) {
        configure( myBoard );
      }

      // SHOW BOARD
      else if( selection.equals( "2" ) ) {
        showBoard( myBoard );
      }

      // ADD A WORD
      else if( selection.equals( "3" ) ) {
        insert( myDictionary );
      }

      // REMOVE A WORD
      else if( selection.equals( "4" ) ) {
        remove( myDictionary );
      }

      // SEARCH FOR WORD
      else if( selection.equals( "5" ) ) {
        search( myDictionary );
      }

      // REPORT SOLUTIONS
      else if( selection.equals( "6" ) ) {
        myBoard.solve( myDictionary );
      }

      // REPORT SOLUTIONS
      else if( selection.equals( "7" ) ) {
        report( myBoard );
      }

      // INVALIDATE A WORD
      else if( selection.equals( "8" ) ) {
        invalidate( myBoard );
      }

      // TALLY SCORE
      else if( selection.equals( "9" ) ) {
        tallyScore( myBoard );
      }

      // SAVE DICTIONARY
      else if( selection.equals( "10" ) ) {

      }

      // EXIT
      else if( selection.equals( "11" ) ) {
        doExit = true;
        exit();
      }

      else if( selection.equals( "12" ) ) {
        verbosity( myBoard, myDictionary );
      }

      else {
        System.out.println( "Unrecognized input. Try again." );
      }
    }
  } // Closing main()




//-------|---------|---------|---------|---------|---------|---------|---------|
// CORE FUNCTIONS
//-------|---------|---------|---------|---------|---------|---------|---------|

  public static void verbosity( BoggleBoard board, TrieDictionary dict ) {
    System.out.println();
    System.out.println();
    System.out.println( "----- VERBOSITY -----" );
    System.out.println();
    Scanner input = new Scanner( System.in );
    System.out.print( "Verbosity (ON/OFF): " );
    String invalidWord = input.nextLine();
    if( input.equals( "ON" ) ) {
      board.setDebugMode( "ON" );
      dict.setDebugMode( "ON" );
    }
    else if( input.equals( "OFF" ) ) {
      board.setDebugMode( "OFF" );
      dict.setDebugMode( "OFF" );
    } 
    else {
      System.out.println( "Unrecognized verbosity setting." );
    }  
  }

  public static void report( BoggleBoard board ) {
    System.out.println();
    System.out.println();
    System.out.println( "----- IDENTIFIED SOLUTIONS -----" );
    System.out.println();
    board.report( );
  }

  public static void configure( BoggleBoard board ) {
    System.out.println();
    System.out.println();
    System.out.println( "----- CONFIGURING BOARD -----" );
    System.out.println();
    board.configure();
  }
  
  public static void showBoard( BoggleBoard board ) {
    System.out.println();
    System.out.println();
    System.out.println( "----- DISPLAYING BOARD -----" );
    System.out.println();
    System.out.println( board.toString() );
  }

  public static void tallyScore( BoggleBoard board ) {
    System.out.println();
    System.out.println();
    System.out.println( "----- TALLYING SCORE -----" );
    System.out.println();
    board.score();
  }

  public static void invalidate( BoggleBoard board ) {
    System.out.println();
    System.out.println();
    System.out.println( "----- INVALIDATING WORD -----" );
    System.out.println();
    Scanner input = new Scanner( System.in );
    System.out.print( "Target word to invalidate: " );
    String invalidWord = input.nextLine();
    board.negate( invalidWord );
  }

  public static void saveDict( TrieDictionary dict ) {
    System.out.println();
    System.out.println();
    System.out.println( "----- SAVING -----" );
    System.out.println();
    Scanner saveInput = new Scanner( System.in );
    System.out.print( "Target filename: " );
    String targetFile = saveInput.nextLine();
    dict.save( targetFile );
  }

  public static void remove( TrieDictionary dict ) {
    System.out.println();
    System.out.println();
    System.out.println( "----- REMOVING WORD FROM DICTIONARY -----" );
    System.out.println();
    Scanner subscanner = new Scanner( System.in );
    System.out.print( "Word to remove: " );
  }

  public static void insert( TrieDictionary dict ) {
    System.out.println();
    System.out.println();
    System.out.println( "----- INSERTING A WORD TO DICTIONARY -----" );
    System.out.println();
    Scanner subscanner = new Scanner( System.in );
    System.out.print( "Insert a word: " );
    String word = subscanner.next().toUpperCase();
    dict.insert( word );
  }

  public static void search( TrieDictionary dict ) {
  Scanner subscanner = new Scanner( System.in );
    System.out.print( "Search for word: " );
    String word = subscanner.next().toUpperCase();
    System.out.print( "Is a word      : " );
    boolean result = dict.isAWord( word );
    if( result == true ) {
      System.out.println("\u001b[32;1mTRUE \u001b[0m");
    }
    else {
      System.out.println("\u001b[31;1mFALSE \u001b[0m");
    }
  }

//-------|---------|
// #exit()
// Desc: How to exit the program
// Args: None
//-------|---------|
  public static void exit() {
    System.out.println();
    System.out.println();
    System.out.println( "----- THANKS FOR PLAYING! -----" );
    System.out.println();
  }

//-------|---------|---------|---------|---------|---------|---------|---------|
// HELPER FUNCTIONS
//-------|---------|---------|---------|---------|---------|---------|---------|

//-------|---------|
// #menu()
// Desc: Display the menu of options
// Args: None
//-------|---------|
  public static void menu() {
    System.out.println();
    System.out.println( " 1 - (BOARD) Configure Boggle board" );
    System.out.println( " 2 - (BOARD) Show board" );
    System.out.println( " 3 - (DICTIONARY) Add word" );
    System.out.println( " 4 - (DICTIONARY) Remove word" );
    System.out.println( " 5 - (DICTIONARY) Query word" );
    System.out.println( " 6 - (BOARD) Calculate solutions" );
    System.out.println( " 7 - (BOARD) Report solutions" );
    System.out.println( " 8 - (BOARD) Invalidate word" );
    System.out.println( " 9 - (BOARD) Tally score" );
    System.out.println( "10 - (DICTIONARY) Save dictionary");
    System.out.println( "11 - Exit" );
    System.out.println( "12 - (GAME) Verbosity toggle" );
    System.out.print(   "Make a selection: " );
  } // Closing menu()

//-------|---------|
// #printGreen()
// Desc: Print an argument in green (no linebreak)
// Args: arg1 - The message to print
//-------|---------|
  private void printGreen( String arg ) {
    System.out.print("\u001b[32;1m");
    System.out.print( arg );
    System.out.print("\u001b[0m");
  } // Closing printGreen()

//-------|---------|
// #printRed()
// Desc: Print an argument in red (no linebreak)
// Args: arg1 - The message to print
//-------|---------|
  private void printRed( String arg ) {
    System.out.print("\u001B[31m");
    System.out.print( arg );
    System.out.print("\u001b[0m");
  } // Closing printRed()

}
