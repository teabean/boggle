import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;
import java.util.Iterator;
import java.util.Collections;
import java.util.LinkedHashSet;

public class BoggleBoard {

//-------|---------|---------|---------|---------|---------|---------|---------|
// CLASS FIELDS
//-------|---------|---------|---------|---------|---------|---------|---------|

  private static boolean DEBUG = false;

  // Width and height of the Boggle board
  private int WIDTH  = 4;
  private int HEIGHT = 4;

  // Possible string combinations found on the dice faces and their storage
  private String FACE_OPTIONS = "R I F O B X I F E H E Y D E N O W S U R O K N D H M S R A O L U P E T S A C I T O A Y L G K U E QU B M J O A E H I S P N V E T I G N B A L I Y T E Z A V N D R A L E S C U W I L R G P A C E M D";
  private ArrayList<String> defaultFaces = new ArrayList<String>();

  // The boggle board
  private String[][] theBoard = new String[WIDTH][HEIGHT];
  // Current path
  private Stack<String> path = new Stack<String>();

  // Identified solutions of a Boggle round and whether they count
  private ArrayList<String> solutions = new ArrayList<String>();
  private boolean[]         doesCount;

//-------|---------|---------|---------|---------|---------|---------|---------|
// CONSTRUCTORS
//-------|---------|---------|---------|---------|---------|---------|---------|

//-------|---------|
// # BoggleBoard( )
// Desc: Default constructor
// Args: None
//-------|---------|
  BoggleBoard( ) {
    this.tare();
    this.theBoard = new String[WIDTH][HEIGHT];

    // Load the default faces
    Scanner optionReader = new Scanner( FACE_OPTIONS );
    while( optionReader.hasNext() ){
      defaultFaces.add( optionReader.next() );
    }

    // Set the tiles at random
    randomize();
  } // Closing BoggleBoard()

//-------|---------|---------|---------|---------|---------|---------|---------|
// CORE FUNCTIONS
//-------|---------|---------|---------|---------|---------|---------|---------|

//-------|---------|
// # solve( arg1 )
// Desc: Solve the board
// Args: arg1 - The dictionary of valid words and prefixes
//-------|---------|
  public void solve( TrieDictionary dict ) {
    if( DEBUG ) {
      System.out.println( "(BoggleBoard)(solve) Solving..." );
    }
    for( int x = 0 ; x < WIDTH ; x++ ) {
      for( int y = 0 ; y < HEIGHT ; y++ ) {
        if( DEBUG ) {
          System.out.println( "(BoggleBoard)(solve) Recurring on [" + x + "][" + y + "]" );
        }
        recursiveSolve( x, y, dict );
      }
    }
    this.trim();
  } // Closing solve()

//-------|---------|
// # recursiveSolve( arg1, arg2, arg3 )
// Desc: Recursive backtrack across the board
// Args: arg1 - The X coordinate to recurse on
//       arg2 - The Y coordinate to recurse on
//       arg3 - The dictionary of valid words
//-------|---------|
  private void recursiveSolve( int x, int y, TrieDictionary dict ) {
    if( DEBUG ) {
      System.out.println( "(BoggleBoard)(RecursiveSolve) [" + x + "][" + y + "]" );
    }
    for( int candidateX = x-1 ; candidateX < x+2 ; candidateX++ ) {
      for( int candidateY = y-1 ; candidateY < y+2 ; candidateY++ ) {
        // If the position is legal, add it to the path and solve from that position
        if( isLegalMove( candidateX, candidateY, this.path, dict ) ) {
          // Append the legal move to the path
          path.push( theBoard[candidateX][candidateY] );
          // Check to see if the path creates a valid word
          String candidateWord = this.pathToString();
          boolean result = dict.isAWord( candidateWord );
          // If the path does create a valid word, add it to the list of found solutions
          if( result == true ) {
            this.solutions.add( candidateWord ); // Will have repeats
            debugPrint( candidateWord );
          }
          // Attempt to recursively solve this problem from the new position
          recursiveSolve( candidateX, candidateY, dict );
          // On the return remove it from the path stack
          path.pop();
        }
        // Otherwise, do not add to the path
        else {
          debugPrint( "(!legal)" );
        }
        // And just move on to the next candidate position

      } // Closing horizontal for loop
    } // Closing vertical for loop
  } // Closing recursiveSolve()

//-------|---------|
// # configure()
// Desc: Reconfigure the Boggle board
// Args: None
//-------|---------|
  public void configure() {
    System.out.println( "Configuring board:" );
    Scanner input = new Scanner( System.in );
    Scanner keyboard = new Scanner( System.in );

    System.out.print( "Board width : " );
    int width = keyboard.nextInt();
    this.setWidth( width );

    System.out.print( "Board height: " );
    int height = keyboard.nextInt();
    this.setHeight( height );

    this.theBoard = new String[ this.WIDTH ][ this.HEIGHT ];

    for( int y = 0 ; y < HEIGHT ; y++ ) {
      System.out.print( "Enter row [" + y + "] (space delimited): ");
      String state = input.nextLine();
      Scanner subscanner = new Scanner( state );
      for( int x = 0 ; x < WIDTH ; x++ ) {
      	this.theBoard[x][y] = subscanner.next();
      }
    }
  } // Closing configure()

//-------|---------|
// #score()
// Desc: Total and report the score tally for this round
// Args: arg1 - Whether a word at an index was unique
//       arg2 - The solutions identified
//-------|---------|
  private void score( boolean[] isUnique, ArrayList<String> solutionArray ) {
    int sum = 0;
    for( int i = 0 ; i < solutionArray.size() ; i++ ) {
      boolean doesCount = isUnique[i];
      String currWord = solutionArray.get(i);
      int points = pointValue( currWord );

      String result = "[" + this.boolToString(doesCount) + "] : ";
      result += String.format( "%-8s ", currWord ); // [1] : ________

      if( isUnique[i] ) {
        sum += points;
        result += String.format( "\u001b[32;1m%-2s \u001b[0m", points );
      }
      else {
        result += "\u001b[31;1m -\u001b[0m";
      }
      System.out.print( String.format( "%-25s ", result ) );

      if( i % 3 == 2 ) {
        System.out.println();
      }
    }
    System.out.println();
    System.out.print( "Total score: " );
    this.printGreen( String.valueOf( sum ) );
    System.out.println();
  } // Closing score()

//-------|---------|---------|---------|---------|---------|---------|---------|
// HELPER FUNCTIONS
//-------|---------|---------|---------|---------|---------|---------|---------|

//-------|---------|
// #boolToString()
// Desc: Convert a boolean from True/False to 1/0
// Args: arg1 - The boolean to represent
//-------|---------|
public String boolToString( boolean b ) {
  if( b == true ) {
    return "1";
  }
  else if( b == false ) {
    return "0";
  }
  return "?";
} // Closing boolToString()

//-------|---------|
// #debugPrint()
// Desc: Print a message if DEBUG mode is on
// Args: arg1 - The message to print
//-------|---------|
  private void debugPrint( String arg ) {
    if( DEBUG ) {
      System.out.println( arg );
    }
  } // Closing debugPrint()

//-------|---------|
// #findAndInvalidate()
// Desc: Invalidate a given solution
// Args: arg1 - The word to invalidate
//       arg2 - The ledger of uniqueness
//       arg3 - The solutions identified
//-------|---------|
  private void findAndInvalidate( String word, boolean[] isUnique, ArrayList<String> solutionArray ) {
    for( int i = 0 ; i < solutionArray.size() ; i++ ) {
      if( solutionArray.get( i ).equals( word ) ) {
        isUnique[ i ] = false;
        this.printRed( word );
        System.out.println( " negated." );
        return;
      }
    }
    System.out.println( "Word not found." );
  } // Closing findAndInvalidate()

//-------|---------|
// # isLegalMove( arg1, arg2, arg3, arg4 )
// Desc: Check the legality of a proposed move
// Args: arg1 - The X-coordinate on the board (0-indexed)
//       arg2 - The Y-coordinate on the board (0-indexed)
//       arg3 - The path of visited during this exploration
//       arg4 - The dictionary of valid words and prefixes
//-------|---------|
  private boolean isLegalMove( int x, int y, Stack<String> path, TrieDictionary dict ) {
    boolean isLegal = true;

    // Bounds check
    if( x < 0 || y < 0 ) {
      if( DEBUG ) {
        System.out.println( "(BoggleBoard)(isLegalMove) Out of bounds (low)" );
      }
      return false;
    }
    if( x >= WIDTH || y >= HEIGHT ) {
      if( DEBUG ) {
        System.out.println( "(BoggleBoard)(isLegalMove) Out of bounds (high)" );
      }
      return false;
    }

    // Path check - Have we been here before?
    Iterator<String> iter = this.path.iterator();
    String currTile = this.theBoard[x][y];
    while( iter.hasNext() ) {
      String pathTile = iter.next();
      if( System.identityHashCode(currTile) == System.identityHashCode(pathTile) ) {
        if( DEBUG ) {
          System.out.println( "(BoggleBoard)(isLegalMove) Repeat path detected @ [" + x + "][" + y + "]" );
        }
        return false;
      }
    }

    // Prefix check - Ask the candidate tile if my path is a valid prefix
    String prefix = this.pathToString();
    if( !dict.isValidPrefix( prefix ) ) {
      return false;
    }
    return isLegal;
  } // Closing isLegalMove()

//-------|---------|
// #pathToString()
// Desc: Convert the path to a String
// Args: None
//-------|---------|
  private String pathToString() {
    String retString = "";
    Stack<String> tempStack = new Stack<String>();
    String currToken = "";

    // Flip the path stack into a temporary stack
    // Z E B R A -> A R B E Z
    while( !this.path.empty() ) {
      tempStack.push( this.path.pop() );
    }
    // Flip the temp stack back, but concatenate to the return as we do so:
    // A R B E Z -> Z E B R A + "ZEBRA"
    while( !tempStack.empty() ) {
      currToken = tempStack.pop();
      retString += currToken;
      this.path.push( currToken );
    }
    return retString;
  } // Closing pathToString()

//-------|---------|
// #pointValue()
// Desc: Calculate the point value of a word
// Args: arg1 - The word to calculate points for
//-------|---------|
  private int pointValue( String word ) {
    int length = word.length();
    if( length > 7 ) {
      return 11;
    }
    else if( length == 7 ) {
      return 5;
    }
    else if( length == 6 ) {
      return 3;
    }
    else if( length == 5 ) {
      return 2;
    }
    return 1;
  } // Closing pointValue()

//-------|---------|
// #printGreen()
// Desc: Print an argument in green (no linebreak)
// Args: arg1 - The message to print
//-------|---------|
  private void printGreen( String arg ) {
    System.out.print("\u001b[32;1m");
    System.out.print( arg );
    System.out.print("\u001b[0m");
  } // Closing printGreen()

//-------|---------|
// #printRed()
// Desc: Print an argument in red (no linebreak)
// Args: arg1 - The message to print
//-------|---------|
  private void printRed( String arg ) {
    System.out.print("\u001B[31m");
    System.out.print( arg );
    System.out.print("\u001b[0m");
  } // Closing printRed()

//-------|---------|
// #randomize()
// Desc: Assigns every face a value selected from a space-delimited string of possibilities
// Args: None
//-------|---------|
  private void randomize() {
    for( int col = 0 ; col < WIDTH ; col++ ) {
      for( int row = 0 ; row < HEIGHT ; row++ ) {
        // Select a random index of all possible faces and grab the string
        int index = (int)(Math.random() * (double)defaultFaces.size());
        String face = defaultFaces.get( index );
        // Assign the randomly selected string to the board
        this.theBoard[col][row] = face;
      }
    }
  } // Closing randomize()

//-------|---------|
// #report()
// Desc: Report the board and solution states
// Args: None
//-------|---------|
  public void report() {
    for( int i = 0 ; i < this.solutions.size() ; i++ ) {
      String entry = "[" + this.boolToString( doesCount[i] ) + "] : " + solutions.get( i );
      System.out.print( String.format( "%-20s ", entry ) );
      if( i % 5 == 4 ) {
        System.out.println();
      }
    }
    System.out.println();
  } // Closing report()

//-------|---------|
// #tare()
// Desc: Zero out all fields
// Args: none
//-------|---------|
  private void tare() {
    for( int col = 0 ; col < WIDTH ; col++ ) {
      for( int row = 0 ; row < HEIGHT ; row++ ) {
        this.theBoard[col][row] = "";
      }
    }
    this.theBoard = null;
    this.defaultFaces.clear();

    this.path.clear();

    this.solutions.clear();
    this.doesCount = null;
  } // Closing tare()

//-------|---------|
// #toString()
// Desc: Returns a string representation of this BoggleBoard
// Args: None
//-------|---------|
  public String toString() {
    String retString = "";
    for( int y = 0 ; y < HEIGHT ; y++ ) {
      for( int x = 0 ; x < WIDTH ; x++ ) {
        retString += String.format( "%-2s ", this.theBoard[x][y] );
      }
      retString += "\n";
    }
    return retString;
  } // Closing toString()

//-------|---------|
// #trim()
// Desc: Remove short words and duplicates from the identified solutions
// Args: None
//-------|---------|
  private void trim() {
    // Solutions should be populated at this point (has repeats and short words)

    // Sort the list to consolidate duplicates next to one another
    debugPrint( "(BOARD)(trim) Sorting..." );
    Collections.sort( this.solutions );

    debugPrint( "(BOARD)(trim) Checking for shorts and dupes..." );

    // For every element in the solutions array... --- STARTING FROM THE BACK! ---
    // Note: Sweeping from the front changes index values for subsequent entries and the final element
    //       Sweeping from the back does not
    int lastIndex = this.solutions.size() - 1;

    for( int i = lastIndex ; i >= 0 ; i-- ) {
      String currAnswer = this.solutions.get(i);
      debugPrint( "Checking: " + currAnswer );

      // If the element is just too short to count, remove it
      if( currAnswer.length() < 3 ) {
        debugPrint( "(BOARD)(solve) Short answer detected, removing: \u001b[31;1m" + currAnswer + "\u001b[0m" );
        this.solutions.remove( i );
      }

      // If we aren't on the last element, we can safely check for repeats at one index higher
      else if( i < lastIndex ) {
        String prevAnswer = this.solutions.get( i + 1 );
        if( currAnswer.equals( prevAnswer ) ) {
          if( DEBUG ) {
            System.out.println( "(BOARD)(solve) Repeat detected" );
            System.out.println( "  PREV: " + prevAnswer );
            System.out.println( "  CURR: " + currAnswer );
            System.out.println( "  Removing: \u001b[31;1m" + currAnswer + "\u001b[0m @ [" + i + "]" );
          }
          this.solutions.remove( i );
        }
      }
      lastIndex = this.solutions.size() - 1;
    } // Closing for loop - trim complete

    // Generate the doesCount array and set them all to true by default
    this.doesCount = new boolean[ this.solutions.size() ];
    for( int i = 0 ; i <= lastIndex ; i++ ) {
      this.doesCount[i] = true;
    }
  } // Closing trim()

//-------|---------|---------|---------|---------|---------|---------|---------|
// SETTERS / GETTERS
//-------|---------|---------|---------|---------|---------|---------|---------|

//-------|---------|
// #negate()
// Desc: The public interface for invalidation of a solution
//       Use when another player has the same word
// Args: arg1 - The word to invalidate
//-------|---------|
  public void negate( String word ) {
    this.findAndInvalidate( word, this.doesCount, this.solutions );
  } // Closing negate()

//-------|---------|
// #negate()
// Desc: Public interface for the scoring
// Args: None
//-------|---------|
  public void score() {
    this.score( this.doesCount, this.solutions );
  }

//-------|---------|
// #setDebugMode()
// Desc: Set the DEBUG mode
// Args: arg1 - "ON" or "OFF" to set DEBUG on or off
//-------|---------|
  public void setDebugMode( String arg ) {
    if( arg.equals( "ON" ) ) {
      this.DEBUG = true;
      debugPrint( "(BOARD) Setting DEBUG to on...");
    }
    else if( arg.equals( "OFF" ) ) {
      debugPrint( "(BOARD) Turning DEBUG off...");
      this.DEBUG = false;
    }
    else {
      System.out.println( "(setDebug) Unrecognized input");
    }
  } // Closing setDebugMode()

//-------|---------|
// #setHeight()
// Desc: Set the height of the board
// Args: arg1 - The board height
//-------|---------|
  public void setHeight( int arg ) {
    this.HEIGHT = arg;
  } // Closing setHeight()

//-------|---------|
// #setWidth()
// Desc: Set the width of the board
// Args: arg1 - The board width
//-------|---------|
  public void setWidth( int arg ) {
    this.WIDTH = arg;
  } // Closing setWidth()

} // Closing class BoggleBoard
