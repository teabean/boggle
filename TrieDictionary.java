import java.util.Scanner;             // For user input
import java.io.File;                  // For file operations
import java.io.FileNotFoundException; // For file exception
import java.io.IOException; // For file exception
import java.io.FileWriter;

public class TrieDictionary {
  private BoggleNode head = new BoggleNode();
  private static boolean DEBUG = false;
  private int minAcceptableLength = 3;

  TrieDictionary() {
  	System.out.println( "Invalid construction. Create by filename." );
  }

  TrieDictionary( String filename ) {
//-------|---------|---------|---------|---------|---------|---------|---------|
// Open the word list file
//-------|---------|---------|---------|---------|---------|---------|---------|
  	if( DEBUG ) {
      filename = "shortdictionary.txt";
  	  System.out.println( "Loading file: " + filename + "..." );
  	}
  	Scanner fileReader = null;
  	try {
  	  File f = new File( filename );
	    if( DEBUG ) {
	      System.out.println( "Checking file open operation:" );
	      System.out.println( "  File name    : " + f.getName()         );
	      System.out.println( "  Path         : " + f.getPath()         );
	      System.out.println( "  Absolute path: " + f.getAbsolutePath() );
	      System.out.println( "  Parent       : " + f.getParent()       );
	      System.out.println( "  Exists       : " + f.exists()          );
	      if( f.exists() ) {
	        System.out.println();
	        System.out.println( "File exists. Checking file states:" );
	        System.out.println( "  Is writeable      : " + f.canWrite()    ); 
	        System.out.println( "  Is readable       : " + f.canRead()     ); 
	        System.out.println( "  Is a directory    : " + f.isDirectory() ); 
	        System.out.println( "  File Size in bytes: " + f.length()      );
	        System.out.println();
	        System.out.println( "Printing file object:" );
	        System.out.println( f );
	      }
	    }
	    fileReader = new Scanner( f );
	  }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }

//-------|---------|---------|---------|---------|---------|---------|---------|
// Load each word
//-------|---------|---------|---------|---------|---------|---------|---------|
    String currToken = "";
    while( fileReader.hasNext() ) {
      currToken = fileReader.next();
      this.insert( currToken );
    }
  }

  public void save( String filename ) {
    debugPrint( "(DICTIONARY)(save) Saving to '" + filename + "'..." );
    File f = new File( filename );

    if( DEBUG ) {
      System.out.println( "Checking file open operation:" );
      System.out.println( "  File name    : " + f.getName()         );
      System.out.println( "  Path         : " + f.getPath()         );
      System.out.println( "  Absolute path: " + f.getAbsolutePath() );
      System.out.println( "  Parent       : " + f.getParent()       );
      System.out.println( "  Exists       : " + f.exists()          );
      if( f.exists() ) {
        System.out.println();
        System.out.println( "File exists. Checking file states:" );
        System.out.println( "  Is writeable      : " + f.canWrite()    ); 
        System.out.println( "  Is readable       : " + f.canRead()     ); 
        System.out.println( "  Is a directory    : " + f.isDirectory() ); 
        System.out.println( "  File Size in bytes: " + f.length()      );
        System.out.println();
        System.out.println( "Printing file object:" );
        System.out.println( f );
      }
    }

    FileWriter fileWriter = null;
    try {
      fileWriter = new FileWriter( filename );

      inOrderTraverseWrite( fileWriter, this.head );

      fileWriter.close();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  private void inOrderTraverseWrite( FileWriter f, BoggleNode currNode ) {
    for( int i = 0 ; i < currNode.getAlphabetLength() ; i++ ) {

    }
  }

  public void remove( String word ) {
    debugPrint( "(DICTIONARY)(remove) Removing '" + word + "'..." );
  }

  public void insert( String word ) {
  	if( this.DEBUG && false ) {
  	  System.out.print( "Insert(" + word + ")  " );
  	}
    this.head.append( word );
  }

//-------|---------|
// #isValidPrefix()
// Desc: Recursively navigate the trie
// Args: arg1 - The prefix to validate
//-------|---------|
  public boolean isValidPrefix( String s ) {
    // Punt this question to the BoggleNode class
  	return head.isValidPrefix( s );
  } // Closing isValidPrefix()

//-------|---------|
// #isAWord()
// Desc: Recursively navigate the trie
// Args: arg1 - The word (or suffix) to find
//-------|---------|
  public boolean isAWord( String word ) {
  	if( DEBUG ) {
      System.out.println( "Checking if word exists: " + word );
  	}
    // e.g. "ZEBRA"
    // From the head, try to select the first letter 'Z'
    BoggleNode currNode = head.getNext( word.charAt(0) );
    if( currNode != null ) {
      // Ask that node if it is the head of 'ZEBRA'
      return currNode.isWordYouHold( word );
    }

    // Otherwise no selection was made and the letter wasn't even on the trie
    return false;
  } // Closing isAWord()

//-------|---------|
// #debugPrint()
// Desc: Print a message if DEBUG mode is on
// Args: arg1 - The message to print
//-------|---------|
  private void debugPrint( String arg ) {
    if( DEBUG ) {
      System.out.println( arg );
    }
  }

//-------|---------|
// #printGreen()
// Desc: Print an argument in green (no linebreak)
// Args: arg1 - The message to print
//-------|---------|
  private void printGreen( String arg ) {
    System.out.print("\u001b[32;1m");
    System.out.print( arg );
    System.out.print("\u001b[0m");
  } // Closing printGreen()

//-------|---------|
// #printRed()
// Desc: Print an argument in red (no linebreak)
// Args: arg1 - The message to print
//-------|---------|
  private void printRed( String arg ) {
    System.out.print("\u001B[31m");
    System.out.print( arg );
    System.out.print("\u001b[0m");
  } // Closing printRed()

//-------|---------|---------|---------|---------|---------|---------|---------|
// SETTERS / GETTERS
//-------|---------|---------|---------|---------|---------|---------|---------|

//-------|---------|
// #setDebugMode()
// Desc: Print a message if DEBUG mode is on
// Args: arg1 - The message to print
//-------|---------|
  public void setDebugMode( String arg ) {
    if( arg.equals( "ON" ) ) {
      this.DEBUG = true;
      this.head.setDebugMode( "ON" );
    }
    else if( arg.equals( "OFF" ) ) {
      this.DEBUG = false;
      this.head.setDebugMode( "OFF" );
    }
    else {
      System.out.println( "(setDebug) Unrecognized input");
    }
  }

}